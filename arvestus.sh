#!/bin/bash
# Loo skript, mis saab käsurealt kolm parameetrit. 1. kataloog 2. kasutajanimi 3. väljundfail
#
# * Skript kontrollib, et kasutajanimi ja kataloog on olemas, vastasel juhul exit 1.
# * Skript kontrollib, kas väljundfaili saab kirjutamiseks avada (või luua), vastasel juhul exit 2.
# * Skript kontrollib, kas parameetreid on täpselt kolm, vastasel juhul exit 3.

# Skript kirjutab väljundfaili kõikide failide nimekirja, kuhu teise parameetrina antud kasutaja saab kirjutada ja mis asuvad esimese parameetrina antud kataloogis või selle alamkataloogides.

# Kui väljundfail on juba olemas, luuakse uus fail mustriga "failinimi_yyyy-mm-dd_HH-MM[.ext]" 
# ehk kui käsurealt antakse failinimi n2idis.txt ja see eksisteerib, on loodava faili nimi n2idis_2016-01-31_12-30.txt

# Funktsioon faili loomiseks
create_file(){ 
  
  FILENAME=$1
  
  touch $1

  if [ $? -ne 0 ]; then
    echo "Faili $FILENAME loomine ebaonnestus"
    exit 2;
  else
    echo "Faili $FILENAME loomine onnestus"
  fi
}

# Parameetrite kontroll
if [ $# -ne 3 ]; then
  echo "Kaivita script $(basename $0) kataloog kasutajanimi valjundfail"
  exit 3
else
  KATALOOG=$1
  KASUTAJA=$2
  VALJUNDFAIL=$3
fi

# Kataloogi kontroll
if [ ! -d $KATALOOG ];then
  echo "Kataloogi: $KATALOOG ei eksisteeri"
  exit 1
fi

# Kasutaja KONTROLL
id -u $KASUTAJA > /dev/null 2>&1; 

if [ $? -ne 0 ];then
  echo "Kasutajat: $KASUTAJA ei eksisteer"
  exit 1
fi

# Valjundfaili loomine, kui valjundfaili ei eksisteeri
if [ ! -f $VALJUNDFAIL ];then

  echo "Faili $VALJUNDFAIL ei eksisteeri. Loome faili"
  
  create_file $VALJUNDFAIL
  
else
  # Uue valjundfaili nime genereerimine
  AEG=`date +%Y-%m-%d_%H-%M`
  VALJUNDFAIL=$(sed "s/\./_$AEG\./" <<< $VALJUNDFAIL)
  echo "Antud fail on juba olemas. Loome uue faili: $VALJUNDFAIL"

  create_file $VALJUNDFAIL
fi 

for file in $(find $KATALOOG -type f -user $KASUTAJA 2>/dev/null);do
  echo $file >> $VALJUNDFAIL 
done