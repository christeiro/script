#!/bin/bash

#Loo skript, mis saab käsurealt kolm parameetrit. 1. Sisendfail 2. Väljundfail 3. otsitav
#* Skript kontrollib, et sisendfail on fail ja loetav. (vastasel juhul exit 1)
#* Skript kontrollib, kas väljundfaili saab kirjutamiseks avada. (vastasel juhul exit 2)
#* Skript kontrollib, kas parameetreid on täpselt kolm (vastasel juhul exit 3)
#* Skript loeb sisendfailist failide ja kataloogide nimekirja (näiteks kujul:
#
#/etc/passwd
#/etc/
#/var/log
#/sedapoleolemas
#
#
#Väljundfaili esimesele reale kirjutatakse 3. parameetrina antud string.
#
#Kui sisendfailist loetud reas on kirjas failinimi, siis otsitakse sisendfailist kolmanda parameetrina antud stringi (3. otsitav). Kui string on failis olemas, kirjutatakse väljundfaili rida: 
# sisendfaili_nimi,OLEMAS vastasel juhul sisendfaili_nimi,POLE OLEMAS
#
#Näiteks, kui sisendfailis oli rida /etc/passwd ja otsitav oli KALASABA, siis väljundfaili kirjutatakse
#/etc/passwd,POLE OLEMAS
#
#Kui sisendfailis oli rida /etc/passwd ja otsitav oli root, siis kirjutatakse väljundfaili
#
#/etc/passwd,OLEMAS
#
#Kui sisendfailis oleval real oli kataloogi nimi, siis otsitakse selle kataloogi seest faile ja katalooge, millede nimes on kolmanda parameetrina antud string (3. otsitav)
#
#Näiteks, kui sisendfailis oli rida /var/log ja otsitav string oli syslog, siis on väljundis
#
#/var/log,OLEMAS
#
#Kui sisendfailis oli rida /var/log ja otsitav string oli sedafailipoleolemas1234567, siis lisatakse väljundfaili rida
#/var/log,POLE OLEMAS

# Seega peab iga sisendfaili rea kohta leiduma väljundfaili rida, mis sisaldab nii sisendfaili rida, kui stringi OLEMAS või POLE OLEMAS.

if [ $# -ne 3 ]; then
  echo "Kaivita script kujul $(basename $0) sisendfail valjundfail"
  exit 3
else
  SISENDFAIL=$1
  VALJUNDFAIL=$2
  OTSITAV=$3
fi

if [[ ! -f $SISENDFAIL &&  ! -r $SISENDFAIL ]] ;then
  echo "$SISENDFAIL pole fail voi pole failil lugemise oigust"
  exit 1
fi

if [ ! -f $VALJUNDFAIL ]; then
  echo "Faili $VALJUNDFAIL ei eksisteeri. Loome faili"
  touch $VALJUNDFAIL

  if [ $? -ne 0 ]; then
    echo "Faili $VALJUNDFAIL loomine ebaonnestus"
    exit 4;
  else
    echo "Faili $VALJUNDFAIL loomine onnestus"
  fi
elif [ ! -w $VALJUNDFAIL ]; then
  echo "Faili $VALJUNDFAIL ei saa kirjutada"
  exit 2
fi

echo $OTSITAV > $VALJUNDFAIL

while read line
do
  if [ -f $line ];then
    COUNT=$(grep $OTSITAV $line | wc -l)
    if [ $COUNT -gt 0 ];then
      echo "$line,OLEMAS" >> $VALJUNDFAIL
    else
      echo "$line,POLE OLEMAS" >> $VALJUNDFAIL
    fi  
  elif [ -d $line ]; then
    # ignoreerime vea teateid, nt: "Permission denied"
    COUNT=$(find $line -name "$OTSITAV" 2>/dev/null | wc -l)
    if [ $COUNT -gt 0 ];then
      echo "$line,OLEMAS" >> $VALJUNDFAIL
    else
      echo "$line,POLE OLEMAS" >> $VALJUNDFAIL
    fi
  else
    echo "$line,POLE OLEMAS" >> $VALJUNDFAIL
  fi
done < $SISENDFAIL

