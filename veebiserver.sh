#!/bin/bash
# Krister Vals
# Veebiserver install. Skript on mõeldud Ubuntu 14.04 versioonile. 
#Vers. tabel

export LC_ALL=C

# Kas õigused on olemas? (UID on 0)
if [ $UID -ne 0 ]
then
  	echo "käivita skript $(basename $0) juurkasutaja õigustes"
  	exit 1
fi

# Kas skriptil on täpselt üks parameeter
if [ $# -eq 1 ]
then
  	DOMAIN=$1 
else 
	echo "Kasuta sktipti $(basename $0) domeen"
    exit 2
fi

#Kontrollin kas apache2 on installeeritud
apt-cache policy apache2 | grep 'Installed: (none)' > /dev/null
if [ $? -ne 1 ]
then
    while :
      do
        read -p "Apache2 ei eksisteeri, kas paigaldan: Jah/Ei: " vastus
        vastus=$(echo $vastus | tr "[:upper:]" "[:lower:]")

        if [ "jah" = "$vastus" -o "j" = "$vastus" ]
        then
          echo "Alustan Apache2 installeerimist"
          apt-get update > /dev/null 2>&1 && apt-get install apache2 -y > /dev/null 2>&1
          if [ $? -ne 0 ]
          then
            echo 'Apache2 install ebaõnnestus'
            exit 3
          else
            echo 'Apache2 edukalt installeeritud!'
            break
          fi
        elif [ "ei" = "$vastus" -o "e" = "$vastus" ]
        then
            echo "Programm katkestas töö, Apache2 paigaldamata"
            exit 4
        fi
      done
fi

# Kontrollib, kas hosts failis on hostname defineeritud (võimalik, et see on välja kommenteeritud)
grep -E "^127.0.0.1    $DOMAIN" /etc/hosts >> /dev/null

if [ $? -eq 0 ]
then
	echo 'DNS varasemalt defineeritud'
else
	echo "127.0.0.1    $DOMAIN" >> /etc/hosts
	echo "Domeen $DOMAIN lisatud >> /etc/hosts"
fi

# check kataloog, loo kataloog
if [ -d "/var/www/$DOMAIN" ]
then
    echo 'Kataloog eksisteerib'
else
    echo 'Loon kataloogi ja genereerin index.html faili'
    mkdir -p "/var/www/$DOMAIN"
    echo "$DOMAIN" >> "/var/www/$DOMAIN/index.html"
fi


# Kontrollin kas apache2 conf fail on olemas, vajadusel lisan.
if [ ! -f "/etc/apache2/sites-available/$DOMAIN.conf" ]
then 
    echo 'Apache2 vhost conf seadistamine'
    sed -e "s/#ServerName www.example.com/ServerName $DOMAIN/" -e "s@DocumentRoot /var/www/html@DocumentRoot /var/www/$DOMAIN@" /etc/apache2/sites-available/000-default.conf > /etc/apache2/sites-available/$DOMAIN.conf
fi

# Kontrollin kas veebileht on enabletud
if [ ! -f "/etc/apache2/sites-enabled/$DOMAIN.conf" ]
then
    echo 'Aktiveerin domeeni'
    a2ensite $DOMAIN > /dev/null
    service apache2 reload
    if [ $? -eq 1 ]
    then
        a2dissite $DOMAIN
        service apache2 reload
        if [ $? -eq 0 ]
        then
            echo 'Apache2 confis tekkis viga'
            exit
        else
            echo 'Apache2 confis tekkis totaalne viga'
            exit
        fi
    else
        echo "$DOMAIN edukalt seadistatud. Head kasutamist!"
        exit 0
    fi
else
    echo "$DOMAIN on varasemalt seadistatud"
    exit 10
fi
