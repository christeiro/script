#!/bin/bash
# Krister Vals
# Antud script teeb järgnevat
#  * Paigaldab samba (kui samba rakendust ei eksisteeri)
#  * loob kausta KAUST (kui KAUST ei eksisteeri)
#  * loob gruppi GRUPP (kui GRUPP ei eksisteeri) 
#  * lisab grupile sobivad smb.conf faili ja teeb failiteenusele reload'i
#  * script eeldab, et SAMBA kasutaja, kes kataloogile ligi peab pääsema on SAMBA's loodud
#  * script eeldab, et SAMBA kasutajal on õigus loodud grupile
# Kasutamine: samba-install-krister.sh /var/KAUST GRUPP [SHARE]
#  * kasutaja peab andma full pathi kataloogile ja kataloogi tee peab algama "/var/"
  
export LC_ALL=C
 
#Kontrollib, kas skript on käivitatud juurkasutajana
if [ $UID -ne 0 ]
then
  echo "käivita skript $(basename $0) juurkasutaja õigustes"
  exit 1
fi
 
#Kontrollib, kas on ette antud õige arv muutujaid

if [ $# -eq 2 ]
then
  KAUST=$1
  GRUPP=$2 
else 
  if [ $# -eq 3 ]
  then
    KAUST=$1
    GRUPP=$2
    SHARE=$3
  else
    echo "kasuta skripti $(basename $0) /var/KAUST GRUPP [SHARE]"
    exit 2
  fi
fi

# Kontrollib kataloogi tee õigsust
COUNTER=$(grep -o "/" <<< "$KAUST" | wc -l)
KAUSTANIMI=$(cut -d"/" -f3 <<< "$KAUST")

if [ $COUNTER -ne 2 ] || [ -z $KAUSTANIMI ]
then
  echo "kasuta skripti $(basename $0) /var/KAUST GRUPP [SHARE]"
  exit 3
fi

# Share'i defineerimine
if [ -z $SHARE ]
then 
  SHARE=$KAUSTANIMI
fi

# Kontrollib, kas samba on paigaldatud (vajadusel paigaldab)
type smbd > /dev/null 2>&1 

if [ $? -ne 0 ]
then
  while :
  do
    read -p "Samba ei eksisteeri, kas paigaldan: Jah/Ei: " vastus
    vastus=$(echo $vastus | tr "[:upper:]" "[:lower:]")

    if [ "jah" = "$vastus" -o "j" = "$vastus" ]
    then
      echo "Alustan samba installeerimist"
      apt-get update > /dev/null 2>&1 && apt-get install samba -y 
      if [ $? -ne 0 ]
      then
        echo 'Samba install ebaõnnestus'
        exit 3
      else
        echo 'Samba installeeritud!'
        break
      fi
    elif [ "ei" = "$vastus" -o "e" = "$vastus" ]
    then
        echo "Programm katkestas töö, samba paigaldamata"
        exit 4
    fi
  done
fi

#Kontrollib, kas grupp on olemas (vajadusel loob)
getent group $GRUPP > /dev/null
if [ $? -ne 0 ]
then
  
  addgroup $GRUPP > /dev/null
  
  if [ $? -ne 0 ]
  then
    
    "Gruppi loomine ebaõnnestus"
    exit 6
  
  else
    echo "Grupp loodud, OK!"
  fi
else
  echo "Grupp olemas!"
fi

#Kontrollib, kas kaust on olemas (vajadusel loob)
if [ -d $KAUST ]
then
  echo "Kataloog eksisteerib!"
else
  mkdir -p $KAUST > /dev/null 2>&1
  if [ $? -ne 0 ]
  then
    echo "Kataloogi loobime ebaõnnestus"
    exit 5
  else
    echo "Loon kataloogi, OK"
  fi
fi

#Kataloogi omaniku vahetamine
chgrp $GRUPP $KAUST

#Kataloogi kirjutamisõiguse muutmine
chmod g+w $KAUST

#Kontrollib, kas samba share on olemas
echo "SHARE: $SHARE"

grep -E "^\[$SHARE\]$" /etc/samba/smb.conf > /dev/null
if [ $? -eq 0 ]
then
  echo 'Share defineeritud, OK'
else
  echo 'Loon Share'
  
  #Teen backup'i vanast failist.
  cp /etc/samba/smb.conf /etc/samba/smb.conf.backup

  cat >> /etc/samba/smb.conf << LOPP
[$SHARE]
    path = $KAUST
    valid users = @$GRUPP
    force group = $GRUPP
    read only = No
    create mask = 0770
    directory mask = 0770
LOPP
  
  # Kontrollin, kas teenuse startimine õnnestus
  service smbd restart > /dev/null 2>&1
  if [ $? -ne 0 ]
  then
    cp /etc/samba/smb.conf.backup /etc/samba/smb.conf
    service smbd restart > /dev/null 2>&1
    if [ $? -ne 0 ]
    then
      echo 'Samba teenuse käivitamine ebaõnnestus totaalselt'
      exit 9
    else
      echo 'Uus samba seadistus ebaõnnestus'
      exit 10
    fi
  else
    rm /etc/samba/smb.conf.backup
    echo 'Protseduur lõpetatud, head sheerimist!'
    exit 0
  fi
fi
